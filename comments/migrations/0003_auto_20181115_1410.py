# Generated by Django 2.0.2 on 2018-11-15 19:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comments', '0002_auto_20181115_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='email',
            field=models.EmailField(max_length=300),
        ),
        migrations.AlterField(
            model_name='comment',
            name='phone_number',
            field=models.IntegerField(max_length=20),
        ),
    ]
