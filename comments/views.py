from django.shortcuts import render, redirect
from .forms import CommentForm

def comment(request):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            succes='Tu mensaje fue enviado'
            return render(request, 'core/contact.html', {'succes':succes})
    return render(request, 'core/contact.html')



# Create your views here.
