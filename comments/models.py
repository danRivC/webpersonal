from django.db import models

# Create your models here.
class Comment(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    email = models.EmailField(max_length=300, blank=False, null=False)
    phone_number = models.IntegerField(max_length=20, blank=False, null=False)
    comment = models.TextField(blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
