from django.shortcuts import render, HttpResponse

navigator = '''
       
    '''
def home(request):

    return render(request, "core/home.html")

def about_me(request):
    return render(request, "core/about_me.html")
def contact(request):

    return render(request, "core/contact.html")
def portfolio(request):
    return render(request, "core/portfolio.html")

# Create your views here.
